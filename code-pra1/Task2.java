//pembuatan class sesuai dengan nama file java
public class Task2 {
    public static void main(String[] args) {

        // Menentukan variable integer
        int age = 25;
        int weight = 48;

        // Membuat IF Statement mengenai usia
        if (age >= 18) {
            // Membuat IF Statement mengenai berat badan
            if (weight > 50) {
                // output code jika berat diatas 50 Kg
                System.out.println("You are eligible to donate blood");
            } else {
                // output code jika berat dibawah 50 Kg
                System.out.println("You are not eligible to donate blood");
            }
        } else {
            // output code jika usia dibawah 18 Tahun
            System.out.println("Age must be greater than 18");
        }
    }
}
// Penjelasan Task2
// Code diatas mengenai sistem donor darah yang menggunakan fungsi IF statement
// Sistem donor darah diatas dicek berdasarkan usia dan berat badan

// IF statement pertama dicek berdasarkan usia. Jika usia diatas
// atau sama dengan 18 Tahun
// Maka pengecekan akan dilanjutkan berdasarkan berat badan.

// Namun jika usia masih dibawah 18 Tahun, maka langsung dinyatakan tidak sesuai
// Dikarenakan usia yang dibutuhkan adalah diatas 18 Tahun

// Jika usia diatas 18 Tahun dan berat badannya diatas 50 Kg
// Maka akan muncul output code yang bertuliskan bahwa peserta memenuhi syarat
// untuk melakukan donor darah

// Jika usia diatas 18 Tahun namun berat badannya dibawah 50 Kg
// Maka akan muncul output code yang bertuliskan bahwa peserta tidak memenuhi
// syarat untuk melakukan donor darah