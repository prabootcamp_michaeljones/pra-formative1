//pembuatan class sesuai dengan nama file java
public class Task1 {
    public static void main(String[] args) {

        // Menentukan variable integer
        int i = 1;

        // Membuat pengulangan menggunakan do-while
        do {
            // membuat If statement
            if (i == 5) {
                i++;
                break;
            }
            // Output code dari integer menggunakan println
            System.out.println(i);
            // Integer diulang
            i++;
            // Persyaratan agar do-while loop dapat berhenti
        } while (i <= 10);
    }

}
// Penjelasan Task1
// Code diatas menjelaskan pembuatan fungsi pengulangan "Do-While" mulai dari 1
// sampai 10 menggunakan variable integer.
// Namun pada pertengahan dari "Do-while", Terdapat "IF Statement" yang dimana
// jika nilai dari variable integer (i) adalah angka 5
// maka code dari "Do-While" berhenti dan hanya menampilkan angka dari 1 - 4
