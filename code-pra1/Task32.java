
//import beberapa fungsi seperti ArrayList dari java.util
import java.util.*;

//pembuatan class sesuai dengan nama file java
public class Task32 {
    public static void main(String args[]) {

        // menentukan ArrayList bertipe string
        ArrayList<String> list = new ArrayList<String>();

        // menambahkan isi-isi dari ArrayList
        list.add("Mango");
        list.add("Apple");
        list.add("Banana");
        list.add("Grapes");

        // output dari ArrayList
        System.out.println(list);
    }
}

// Penjelasan Task31
// Code diatas mengenai ArrayList buah-buahan
// Perbedaan dari Task31 dan Task32 ada pada jenis penggunakan Arraynya
// Pada Task32, Sistem pemanggilan yang digunakan hanya menggunakan
// System.out.println.

// yang dimana output dari arraylist diatas tidak bersifat pengulangan
// output dari arraylist diatas hanya bersifat menyebutkan dari apa yang ada
// di dalam list tersebut tanpa mengalami pengulangan
