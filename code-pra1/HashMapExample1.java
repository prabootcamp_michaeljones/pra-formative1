import java.util.*;

public class HashMapExample1 {
    public static void main(String args[]) {
        HashMap<Integer, String> map = new HashMap<Integer, String>();
        map.put(1, "Mango");
        map.put(2, "Apple");
        map.put(3, "Banana");
        map.put(4, "Grapes");
        map.put("pisang", "Pisang");
        // map.put(5, "Pisang");
        System.out.println("Iterating Hashmap...");
        for (Map.Entry m : map.entrySet()) {
            System.out.println(m.getKey() + " " + m.getValue());
        }
    }
}

// Penjelasan HashMapExample1
// Code diatas menjelaskan Iterasi Hashmap dari Buah-buahan
// Namun ketika code dijalankan, terjadi error pada line 10
// Hal tersebut dikarenakan input Hashmap pada Array ke 5
// Hashmap diatas sudah diatur inputnya dengan <Int, String>
// Namun yang terjadi pada Hashmap ke 5 adalah <String, String>

// Maka jika ingin program dari hashmap ini berjalan
// Array Hashmap ke 5 harus diubah menjadi <Int, String>
// Seperti <5, Pisang>
