
//import beberapa fungsi seperti Scanner dari java.util
import java.util.*;

public class Task4 {
    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        int a, b, c;
        System.out.print("Masukan Angka Pertama: ");
        a = input.nextInt();
        System.out.print("Masukan Angka Kedua: ");
        b = input.nextInt();
        c = a + b;

        System.out.println("Hasil Penjumlahan dari " + a + " dan " + b + " adalah: " + c);
    }
}
