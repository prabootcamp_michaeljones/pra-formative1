
//import beberapa fungsi seperti ArrayList dari java.util
import java.util.*;

//pembuatan class sesuai dengan nama file java
public class Task31 {
  public static void main(String args[]) {

    // menentukan ArrayList bertipe string
    ArrayList<String> list = new ArrayList<String>();
    // menambahkan isi-isi dari ArrayList
    list.add("Mango");
    list.add("Apple");
    list.add("Banana");
    list.add("Grapes");

    // Penerapan fungsi Iterator pada ArrayList
    Iterator itr = list.iterator();

    // Penerapan fungsi While pada iterator
    while (itr.hasNext()) {
      // output jika iterator array memiliki data selanjutnya
      System.out.println(itr.next());
    }
  }
}

// Penjelasan Task31
// Code diatas mengenai ArrayList buah-buahan
// Perbedaan dari Task31 dan Task32 ada pada pemanggilan dari Arraynya
// Task31 menggunakan Iterator Yang dimana Iterator digunakan untuk melakukan
// iterasi pada collection, seperti ArrayList

// Dan pada fungsi akhir tersebut berupa pengulangan dari fungsi While
// yang dimana setelah Iterator telah diterapkan di ArrayList
// maka jika terdapat data selanjutnya dalam list tersebut
// akan ada pengulangan pada System.out.println sampai list tersebut tidak ada
// lagi data selanjutnya
